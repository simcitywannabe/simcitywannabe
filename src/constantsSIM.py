
#CONSTANTS DE SISTEMA
casellesAmple = 10
casellesAlt = 10

temps_step = 0.75

#CONSTANTS DE TAMANYS
SIMample = 900
SIMalt = 600

gridAmple = SIMample - (SIMample / 3)
gridAlt = SIMalt - (SIMalt / 5)

espaiAmple = gridAmple / casellesAmple
espaiAlt = gridAlt / casellesAlt

ampleButton = 100
altButton = 50

#CONSTANTS DE MARGES
gridMargeEsq = 20
gridMargeSup = 30

startButtonsMargeEsq = gridMargeEsq + gridAmple + 40
startButtonsMargeSup = gridMargeSup + 30

startButtonsBuildingsSup = startButtonsMargeSup + 350

infoTextMargeSup = 200

#CONSTANTS DE COLORS
back_color = (79, 232, 65)
grid_color = (232, 187, 65)
grid_selected = (150, 110, 2)
button_color = (0, 209, 132)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

edifici1 = (200, 10, 10)
edifici2 = (20, 10, 50)
edifici3 = (50, 100, 0)
edifici4 = (60, 0, 40)
edifici5 = (100, 200, 200)
edifici6 = (100, 100, 60)

edifici_color = ((200, 10, 10), (20, 10, 200), (100, 200, 200), (100, 100, 60), (50, 0, 100), (60, 0, 40))

#CONSTANTS DE NOMS
edifici_nom_ab = ("Hab", "Com", "Esc", "Cen", "NaN", "NaN")
edifici_nom = ("Habitatge", "Comerc", "Escola", "C. Energia")
