
class grid():
	#la matriu en la qual es desenvolupa l'accio
	files = 0
	columnes = 0
	matriu = None
	
	def __init__(self, rows, cols):
		self.files = rows;
		self.columnes = cols;
		self.matriu = [[None for i in range(rows)] for j in range(cols)]

	def __setitem__(self, position, value):
		self.matriu[position.x][position.y]= value

	def __getitem__(self, position):
		return self.matriu[position.x][position.y]

	def __delitem__(self, position):
		del self.matriu[position.x][position.y]

	def getPerformance(self, position):
		if self.isBusy(position):
			return self.matriu[position.x][position.y].performance
		return 0

	def isBusy(self, position):
		if self.belong(position.x, position.y):
			return self.matriu[position.x][position.y] != None

	def getBuild(self, position):
		return self.matriu[position.x][position.y]

	def printGrid(self):
		for i in range(self.files):
			print self.matriu[i]

	def getFiles(self):
		return self.files

	def getColumnes(self):
		return self.columnes

	def getInfo(self):
		info = [{},{},{}];
		for i in range(self.files):
			for j in range(self.columnes):
				if self.matriu[i][j] != None:
					auxInfo = self.matriu[i][j].getInfo()
					
					for a in range(0,3):
						for element in auxInfo[a]:
							if not (element in info[a]):
								info[a][element] = 0
							info[a][element] += auxInfo[a][element]
		return info


	def belong(self, fila, columna):
	#retorna CERT si la casella filaXcolumna pertany a la matriu
		if fila < 0 or fila > self.files -1:
			return False
		if columna < 0 or columna > self.columnes -1:
			return False
		return True

	def isBorderland(self, fila, columna):
	#retorna CERT si la casella filaXcolumna esta a l'extrem de la grid
		if not self.belong(fila, columna):
			return False

		if fila == 0 or columna == 0 or fila == self.files-1 or columna == self.columnes-1:
			return True

		return False

_directions = {'top': {'x':-1,  'y':0},'bottom': {'x':1, 'y':0},
			   'left': {'x':0, 'y':-1},'right': {'x':0,  'y':1},
			   'top-left': {'x':-1,  'y':-1},'top-right': {'x':-1, 'y':1},
			   'bot-left': {'x':1, 'y':-1},'top-right': {'x':1,  'y':1}}

class Point:
	def __init__(self, x, y):
		self.x, self.y = x, y

	def nextStep(self, direction):
		self.x += _directions[direction]['x']
		self.y += _directions[direction]['y']

	def add(self, other):
		return Point(self.x + other.x, self.y + other.y)

	def copy(self):
		return Point(self.x, self.y)

	def println(self):
		print ("Position: (%d, %d)" % (self.x, self.y))