import random

numHabitatges = [25,50,25,50,25,50,25,50,25,50,25,50,25,50,25,50]
numComercos =   [ 3, 3, 6, 6, 3, 3, 6, 6, 3, 3, 6, 6, 3, 3, 6, 6]
numEscoles =    [ 3, 3, 3, 3, 6, 6, 6, 6, 3, 3, 3, 3, 6, 6, 6, 6]
numCentrals =   [ 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 4]

caselles = range(100)

for cas in range(16):
	file = open("cas" + str(cas + 1) + ".txt", "w+")
	random.shuffle(caselles)
	escollits = 0
	
	for habitatge in range(numHabitatges[cas]):
		valor = caselles[escollits]
		file.write(str((valor / 10) + 1) + " " + str((valor % 10) + 1) + " 1\n")
		escollits = escollits + 1
	for comerc in range(numComercos[cas]):
		valor = caselles[escollits]
		file.write(str((valor / 10) + 1) + " " + str((valor % 10) + 1) + " 2\n")
		escollits = escollits + 1
	for escola in range(numEscoles[cas]):
		valor = caselles[escollits]
		file.write(str((valor / 10) + 1) + " " + str((valor % 10) + 1) + " 3\n")
		escollits = escollits + 1
	for central in range(numCentrals[cas]):
		valor = caselles[escollits]
		file.write(str((valor / 10) + 1) + " " + str((valor % 10) + 1) + " 4\n")
		escollits = escollits + 1
	
	
	