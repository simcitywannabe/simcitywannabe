import pygame, sys
from constantsSIM import *
from pygame import *
import Buttons
import threading

from SIMController import *

button_start = Buttons.Button()
button_put_building = Buttons.Button()

button_edifici1 = Buttons.Button()
button_edifici2 = Buttons.Button()
button_edifici3 = Buttons.Button()
button_edifici4 = Buttons.Button()
#button_edifici5 = Buttons.Button()
#button_edifici6 = Buttons.Button()

button_del = Buttons.Button()

list_grid = { (i,j):-1 for i in range(0, casellesAmple) for j in range(0, casellesAlt) }
list_grid_produccio = { (i,j):-1 for i in range(0, casellesAmple) for j in range(0, casellesAlt) }

controller = SIMController(casellesAmple,casellesAlt)

button_edifici_gran = Buttons.Button()

casella_seleccionada = (-1, -1)

started = False
show_botons_edificis = False
seleccionat = False
quin = -1

showing_info_casella = False

text_button_start = "Start"

global_coord = (-1, -1)


def pinta_loop(windows):
	windows.fill(back_color)
		
	grid_area = pygame.Rect(gridMargeEsq, gridMargeSup, gridAmple, gridAlt)

	draw.rect(windows, grid_color, grid_area)

	pinta_edificis(windows)
	pinta_casella_seleccionada(windows)

	for i in range(0, casellesAmple+1):
		start_point = (gridMargeEsq + i*espaiAmple, gridMargeSup)
		end_point = (gridMargeEsq + i*espaiAmple, gridMargeSup + gridAlt)
		draw.line(windows, BLACK, start_point, end_point, 2) 

	for i in range(0, casellesAlt+1):
		start_point = (gridMargeEsq, gridMargeSup + i*espaiAlt)
		end_point = (gridMargeEsq + gridAmple, gridMargeSup + i*espaiAlt)
		draw.line(windows, BLACK, start_point, end_point, 2) 

	pinta_buttons(windows)
	pinta_menu_edificis(windows)
	pinta_edifici_info(windows)
	pinta_info_caselles(windows)
	pinta_text_edifici(global_coord, windows)

def pinta_info_caselles(windows):
	#aqui pintem la produccio o altra info rellevant que podem trobar a 
	#list_grid_produccio a cada tick de rellotge (aquesta info l'actualitza
	#el motor de simulacio en si, aixo nomes es un visor)
	for columna in range(0, casellesAmple):
                for fila in range(0, casellesAlt):
                        if list_grid[columna, fila] != -1:
				pinta_info_casella(windows, columna, fila, list_grid_produccio[columna, fila])
	
def pinta_info_casella(windows, c, f, valor):
	if valor == -1:
		return
	font = pygame.font.SysFont("Calibri", 20)
        text = font.render(str(valor), 1, BLACK)
        windows.blit(text, (gridMargeEsq + (c)*espaiAmple+1 + 10, gridMargeSup + (f)*espaiAlt+1 + 10))

def pinta_edifici_gran(windows, index):
	button_edifici_gran.create_button(windows, edifici_color[quin-1], startButtonsMargeEsq + (3*50 + 20*3)/4, startButtonsBuildingsSup - 50, 100, 100, 2, edifici_nom_ab[index], BLACK)

def pinta_edifici_info(windows):
	#aqui pintem una icona en gran e informacio sobre l'edifici (nom, atributs...)
	if quin != -1:
		pinta_edifici_gran(windows, quin-1)
		font = pygame.font.SysFont("Calibri", 35)
		text = font.render(edifici_nom[quin-1], 1, BLACK)
		windows.blit(text, (startButtonsMargeEsq + (3*50 + 20 * 3)/4, startButtonsBuildingsSup + 60))
		

def pinta_buttons(windows):
	button_put_building.create_button(windows, button_color, startButtonsMargeEsq, startButtonsMargeSup, ampleButton, altButton, 2, "Put Building", BLACK)
	button_start.create_button(windows, button_color, startButtonsMargeEsq, startButtonsMargeSup + 10 + altButton, ampleButton, altButton, 2, text_button_start, BLACK)	

def pinta_casella_seleccionada(windows):
	if casella_seleccionada[0] == -1 or casella_seleccionada[1] == -1 or started == True:
		return
	else:
		s = pygame.Rect(gridMargeEsq + (casella_seleccionada[0]-1)*espaiAmple+1, gridMargeSup + (casella_seleccionada[1]-1)*espaiAlt+1, espaiAmple, espaiAlt)
		draw.rect(windows, grid_selected, s)

def pinta_casella(windows, f, c, color):
	s = pygame.Rect(gridMargeEsq + (f)*espaiAmple+1, gridMargeSup + (c)*espaiAlt+1, espaiAmple, espaiAlt)
	draw.rect(windows, edifici_color[color], s)

def pinta_botons_edificis(windows):
	#aqui si carreguem edificis de manera dinamica haurem de llegir un edifici o quelcom.
	
	if not show_botons_edificis or started == True:
		return

	button_edifici1.create_button(windows, edifici_color[0], startButtonsMargeEsq, startButtonsBuildingsSup, 50, 50, 2, "Hab", BLACK)

	button_edifici2.create_button(windows, edifici_color[1], startButtonsMargeEsq + 70, startButtonsBuildingsSup, 50, 50, 2, "Com", BLACK)

	button_edifici3.create_button(windows, edifici_color[2], startButtonsMargeEsq, startButtonsBuildingsSup + 70, 50, 50, 2, "Esc", BLACK)

	button_edifici4.create_button(windows, edifici_color[3], startButtonsMargeEsq + 70, startButtonsBuildingsSup + 70, 50, 50, 2, "Cen", BLACK)

	button_del.create_button(windows, WHITE, startButtonsMargeEsq + 140, startButtonsBuildingsSup, 50, 120, 2, "DEL", BLACK)

	#button_edifici5.create_button(windows, edifici_color[4], startButtonsMargeEsq + 70, startButtonsBuildingsSup + 70, 50, 50, 2, "Ed5", BLACK)
	
	#button_edifici6.create_button(windows, edifici_color[5], startButtonsMargeEsq + 140, startButtonsBuildingsSup + 70, 50, 50, 2, "Ed6", BLACK)


def pinta_menu_edificis(windows):
	if casella_seleccionada[0] == -1 or casella_seleccionada[1] == -1 or not seleccionat:
		return
	else:
		pinta_botons_edificis(windows)
		global show_botons_edificis
                show_botons_edificis = True

def pinta_edificis(windows):
	for columna in range(0, casellesAmple):
		for fila in range(0, casellesAlt):
			if list_grid[columna, fila] != -1:
				pinta_casella(windows, columna, fila, list_grid[columna, fila]-1)	

def pinta_text_edifici(cf, windows):
	#mostra info de la casella a la que hem apretat
	if not showing_info_casella:
		return
	font = pygame.font.SysFont("Calibri", 25)
        text = font.render("Casella " + str(cf[0]-1) + " - " + str(cf[1]-1), 1, BLACK)
        windows.blit(text, (startButtonsMargeEsq, infoTextMargeSup))

	if list_grid[cf[0]-1, cf[1]-1] != -1:
		font = pygame.font.SysFont("Calibri", 22)
		text = font.render(edifici_nom[list_grid[cf[0]-1, cf[1]-1]-1], 1, BLACK)
		windows.blit(text, (startButtonsMargeEsq, infoTextMargeSup + 25))

		pinta_atributs_edifici(cf[0]-1, cf[1]-1, windows)

def pinta_atributs_edifici(c, f, windows):
	#pinta la info dels atributs d'un edifici
	#situat a [c, f]. A aquests valors JA se'ls hi
	#ha restat 1, aixi que comencen en [0,0]

	#TODO: FRANZI, AQUI S'HAN DE FER COSES PER TREURE LA INFO EN CONCRET
	
	#anem a asumir que tindrem una tupla amb DUES (2) linies
	#d'info	que printarem a sota de la info que ja tenim
	info = controller.getInformation(c,f)


	#info = ("Bla: Bla bla 1", "alB: 2 alb alB")

	font = pygame.font.SysFont("Calibri", 20)
        text = font.render(info[0], 1, BLACK)
        windows.blit(text, (startButtonsMargeEsq, infoTextMargeSup + 50))

        text = font.render(info[1], 1, BLACK)
        windows.blit(text, (startButtonsMargeEsq, infoTextMargeSup + 70))

        text = font.render(info[2], 1, BLACK)
        windows.blit(text, (startButtonsMargeEsq, infoTextMargeSup + 90))
	

#####################

def get_grid():
	#retorna quin edifici hi ha a cada posicio de la grid
	for columna in range(0, casellesAmple):
                for fila in range(0, casellesAlt):
			if list_grid[columna, fila] == -1:
				print 'fila: ', fila, ', columna: ', columna, ' - RES'
			else:
				print 'fila: ', fila, ', columna: ', columna, ' - ', list_grid[columna, fila]

def add_building():
	
	#si quin = -1 vol dir que hem d'esborrar edifici

	global quin
	global list_grid
	list_grid[casella_seleccionada[0]-1, casella_seleccionada[1]-1] = quin

	if (quin != -1):
		controller.addBuilding(quin, casella_seleccionada[0]-1, casella_seleccionada[1]-1)
	else:
		controller.deleteBuilding(casella_seleccionada[0]-1, casella_seleccionada[1]-1)

	print list_grid[casella_seleccionada[0]-1, casella_seleccionada[1]-1]
	
	global casella_seleccionada
	quin = -1
	casella_seleccionada = (-1, -1)

def check_buttons():
	if button_start.pressed(pygame.mouse.get_pos()):
		if (not started):
			global text_button_start
			text_button_start = "Pause"			

			global started
			started = True
			quin = -1

			get_grid()

			sim()
		else:	
			global text_button_start
			text_button_start = "Start"

			global started
			started = False

		return
	if button_put_building.pressed(pygame.mouse.get_pos()):
		if quin != -1:
			add_building()
		return
	if show_botons_edificis and started != True:
		global show_botons_edificis
		global seleccionat
		global quin
		if button_edifici1.pressed(pygame.mouse.get_pos()):
			print("edifici1!")
			show_botons_edificis = False
			seleccionat = False
			quin = 1
			return
		
		if button_edifici2.pressed(pygame.mouse.get_pos()):
			print("edifici2!")
			show_botons_edificis = False
			seleccionat = False
			quin = 2
			return
		
		if button_edifici3.pressed(pygame.mouse.get_pos()):
			print("edifici3!")
			show_botons_edificis = False
			seleccionat = False
			quin = 3
			return
		
		if button_edifici4.pressed(pygame.mouse.get_pos()):
			print("edifici4!")
			show_botons_edificis = False
			seleccionat = False
			quin = 4
			return

		if button_del.pressed(pygame.mouse.get_pos()):
			print("DEL")
			global quin
			quin = -1
			add_building()
				
		
		'''if button_edifici5.pressed(pygame.mouse.get_pos()):
			print("edifici5!")
			show_botons_edificis = False
			seleccionat = False
			quin = 5
			return
		
		if button_edifici6.pressed(pygame.mouse.get_pos()):
			print("edifici6!")
			show_botons_edificis = False
			seleccionat = False
			quin = 6
			return'''
	


def es_casella(mouse_x,mouse_y, x, y):
	#retorna cert si les coordenades mouse_x/mouse_y estan dins d'una casella i/j de la grid
	for i in range(gridMargeEsq, gridMargeEsq + x*espaiAmple, espaiAmple):
		for j in range(gridMargeSup, gridMargeSup + y*espaiAlt, espaiAlt):
			if mouse_x > i:
				if mouse_y > j:
					if mouse_x < i+espaiAmple:
						if mouse_y < j+espaiAlt:
							return True
	return False

def check_grid():
	mouse = pygame.mouse.get_pos();
	print(mouse[0], " --- ", mouse[1])
	for columna in range(1, casellesAmple+1):
		for fila in range(1, casellesAlt+1):
			if es_casella(mouse[0], mouse[1], columna, fila) and not started:
				#nomes selecciones si no s'ha iniciat la partida
				print('click a ', mouse[0], '--', mouse[1], '. Casella. Columna: ', columna, ', Fila: ', fila)
				global casella_seleccionada
				casella_seleccionada = (columna, fila)
				print casella_seleccionada[0], ' >< ', casella_seleccionada[1]

				global seleccionat
				seleccionat = True

				global quin
				quin = -1

				#return
			if es_casella(mouse[0], mouse[1], columna, fila):
				#aqui fem check per mostrar info

				global showing_info_casella
				showing_info_casella = True

				global global_coord
				global_coord = (columna, fila)

				return

def check_events(windows):
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			pygame.quit()
		if event.type == MOUSEBUTTONDOWN:
			check_grid()
			check_buttons()

def sim():
	if (not started):
		return
	if (started):
		threading.Timer(temps_step, sim).start()
		controller.simulationStep()

		for i in range(casellesAlt):
			for j in range(casellesAmple):
				if (list_grid[i,j] != -1):
					#value = "%10.2f%" % (controller.getPerformance(i,j)*100)
					list_grid_produccio[i,j] = ("%.2f" % (controller.getPerformance(i,j)*100))


def main():
	pygame.init()

	fpsClock = pygame.time.Clock()
	
	windows = pygame.display.set_mode((SIMample, SIMalt))
	pygame.display.set_caption("SIM CITY WANNABE")

	controller.start()
	#imagen = pygame.image.load("/home/koopa/Baixades/koopa_av2.png")
	
	if (sys.argv[1] != None):
	  f = open(sys.argv[1], 'r')
	  for line in f:
	    casella = line.split(' ')
	    global casella_seleccionada
	    casella_seleccionada = (int(casella[0]), int(casella[1]) )
	    global quin
	    quin = int(casella[2])
	    add_building()

	while True:	
		pinta_loop(windows)
		
		check_events(windows)

		fpsClock.tick(30)
		pygame.display.flip()

if __name__ == "__main__":
	main()
