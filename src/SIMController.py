import random

# from SimPy.Simulation import Globals, now, initialize, activate, SimEvent, simulate, cancel

from SimPy.Simulation import *

from City import Clock, Building, setCityGrid, getCity, StepCtrl

from scwClases import grid, Point
import os

from datetime import datetime

class SIMController:
  
  def __init__(self, gridX, gridY):
    self.ILLA_HABITATGES = 1
    self.ILLA_COMERCIAL = 2
    self.ESCOLA = 3
    self.CENTRAL_ELECTRICA = 4
    setCityGrid(gridX, gridY)
    initialize() 
    random.seed(0)
    self.count = 0
    self.activated = False

    #Controlador de steps.
    self.steps = StepCtrl(Globals)
    self.fileCtrl = open('Results-'+datetime.now().isoformat(),'w')


  def __del__(self):
    self.fileCtrl.close()

  def _newIllaHabitatges(self, position):
    produce = {'PE': random.gauss(578, 192)} #distribucio normal de persones
    consume = {'EN': 1587.52} #en kWh*1000/mes
    b = Building(position, consume, produce, 1, "Habitatge"+str(position.x) + '-' + str(position.y) )
    return b

  def _newIllaComercial(self, position):
    produce = {}
    consume = {
      'EN': random.gauss(1553.4695, 740.5675), # distribucio normal en kWh/mes
      'PE': 1000
    }
    b = Building(position, consume, produce, 1, "Comerc"+str(position.x) + '-' + str(position.y) )
    return b

  def _newEscola(self, position):
    produce = {}
    consume = {
      'EN': 2511.648, # en kWh*1000/mes
      'PE': 2161
    }
    b = Building(position, consume, produce, 12, "Escola"+str(position.x) + '-' + str(position.y) )
    return b

  def _newCentralElectrica(self, position):
    produce = {'EN': 7000000} # en kWh*1000/mes
    consume = {'PE': 50}
    b = Building(position, consume, produce, 1, "Central"+str(position.x) + '-' + str(position.y) )
    return b

  def getPerformance(self, x, y):
    performance = 0
    pos = Point(x,y)
    if(getCity().isBusy(pos)):
      performance = getCity().getPerformance(pos)

    return performance

  def getInformation(self, x, y):
    info = None
    pos = Point(x,y)
    if(getCity().isBusy(pos)):
      info = getCity()[pos].getInformation()
    return info

  def addBuilding(self, buildingType, gridX, gridY):
    position = Point(gridX, gridY)
    b = None
    if (buildingType == self.ILLA_HABITATGES):
      b = self._newIllaHabitatges(position)
    elif (buildingType == self.ILLA_COMERCIAL):
      b = self._newIllaComercial(position)
    elif (buildingType == self.ESCOLA):
      b = self._newEscola(position)
    else:
      b = self._newCentralElectrica(position)
    
    activate(b, b.produce())
    getCity()[position] = b

  def simulationStep(self):
    self.steps.nextStep()
    info = getCity().getInfo()

    self.fileCtrl.write('Resources , ')
    for element in info[0]:
      self.fileCtrl.write(element + ' , ' + str(info[0][element]) + ", ")

    self.fileCtrl.write('Consume , ')
    for element in info[1]:
      self.fileCtrl.write(element + ' , ' + str(info[1][element]) + ", ")

    self.fileCtrl.write('Generate , ')
    for element in info[2]:
      self.fileCtrl.write(element + ' , ' + str(info[2][element]) + ", ")

    self.fileCtrl.write('\n')



  def start(self):
    if not self.activated:
      self._clock = Clock()
      activate(self._clock, self._clock.TimeOut()) 
      self.activated = True

  def deleteBuilding(self, x, y):
      pos = Point(x, y)
      self._clock.cancelProcess(getCity()[pos])
      #Al no tenir cap referencia de l'agent i 
      #haver cancelat el process s'elimina sol.
      getCity()[pos] = None
