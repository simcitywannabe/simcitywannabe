from __future__ import division

from SimPy.SimulationStep import (Lister, Process, Store, queueevent,
	activate, reactivate, get, hold,initialize, now, put, 
	simulate, SimEvent, passivate, waitevent)

from scwClases import grid, Point, _directions
from threading import Lock

cityGrid = None

def setCityGrid(Rows, Cols):
	global cityGrid
	cityGrid = grid(Rows, Cols)

def getCity():
	return cityGrid


class Building(Process):
	
	def __init__(self,position,consume,generate, timeOut = 1, name = ""):
		self.timeOut = timeOut
		self.position = position.copy()
		self.consume = consume
		self.generate = generate
		self.resources = {}
		self.lock = Lock()
		self.count = 0
		self.performance = 0
		self.deleted = False

		for i in consume:
			self.resources[i] = self.consume[i]

		Process.__init__(self, name=name)

	def __del__(self):
		print ('Deleted Building [%s]' % self.name)
		
	def getInformation(self):
		produce = '[+ +]:   '
		for item in self.generate:
			produce += (' %s: %.2f' % (item, self.generate[item]))
		consum  = '[ - - ]:   '
		for item in self.consume:
			consum += (' %s: %.2f' % (item, self.consume[item]))
		conte   = '[Now]: '
		for item in self.resources:
			conte += (' %s: %.2f' % (item, self.resources[item]))
		return (produce, consum, conte)

	def getInfo(self):
		return (self.resources, self.consume, self.generate)

	def receive(self, resources):
		self.lock.acquire()
		for i in resources:
			if i in self.consume:
				needed = self.consume[i] - self.resources[i]
				needed = min(resources[i], needed) 
				if needed > 0:
					self.resources[i] += needed
					resources[i] -= needed
		self.refreshPerformance()
		self.lock.release()

	def refreshPerformance(self):
		self.performance = 1.0
		for i in self.consume:
			self.performance = min(self.performance, self.resources[i]/self.consume[i])

	def send(self):
		self.lock.acquire()
		product = {}
		
		self.refreshPerformance()

		for i in self.consume:
			self.resources[i] -= self.consume[i]*self.performance
		
		for i in self.generate:
			product[i] = self.generate[i]*self.performance

		self.lock.release()
		return product


	def produce(self):
		while True:
			yield waitevent, self, timer #El timeout es controlat pel Clock
			self.count += 1

			if self.deleted:
				break

			if self.count == self.timeOut:
				self.count = 0
				#print ("[%s] Produeix en el temps [%s] amb performance [%d]" % (self.name, now(), self.performance*100))

				product = self.send()
				if self.performance > 0:
					for dir in _directions:
						garbage = GarbageCollector(direction=dir, resources=product,
												   position=self.position, name=self.name)
						activate(garbage, garbage.sendGarbage())
				#else:
				#	print ('[%s] need more resources' % (self.name))

class Clock(Process):

	def __init__(self, timeOut = 1):
		self.timeOut = timeOut
		Process.__init__(self, name="Clock")

	def TimeOut(self):
		while True:
			yield hold, self, self.timeOut
			print('[%s]: ' % (now()))
			timer.signal()

	def cancelProcess(self, p):
		p.deleted = True
		self.cancel(p)

class GarbageCollector(Process):

	def __init__(self, direction, position, name, resources = {}):
		Process.__init__(self, name="Cl-"+str(now())+":"+name+" | "+direction)
		self.direction = direction
		self.position = position.copy()
		self.resources = resources.copy()

	def sendGarbage(self):
		while True:
			yield waitevent, self, timer
			self.position.nextStep(self.direction)

			if cityGrid.belong(self.position.x, self.position.y):
				if cityGrid.isBusy(self.position):
					cityGrid.getBuild(self.position).receive(self.resources)

			else:
				break

class StepCtrl:
	def __init__(self, glob):
		self.asim = glob.sim
		self._time = 0

	def nextStep(self):
		self._time += 1
		print('Step: [%s] - [%d]' % (now(), self._time))
		while self.asim.peek() < self._time:
			self.asim.step()

timer = SimEvent('TimerClock')
"""
TIME = 30
city = grid(10,10)

initialize()
timer = SimEvent('TimerClock')

_clock = Clock()
activate(_clock, _clock.TimeOut())

consume = {'water':10, 'energy':50, 'people':5}
consume2= {'water':10, 'energy':10}
consume4= {'people': 40}

generate= {'water':5, 'energy': 60, 'people':10}
generate2= {'people':5}
generate3= {'people':20, 'water':10, 'energy':50}
generate4= {'water':10}

position = Point(5,5)
position2 = Point(8,5)
position3 = Point(2,2)
position4 = Point(8,2)
b = Building(timeOut=5, name="B01", position=position, consume=consume, generate=generate)
b2 = Building(timeOut=5, name="B02", position=position2, consume=consume2, generate=generate2)
b3 = Building(timeOut=10, name="B03", position=position3, consume={}, generate=generate3)
b4 = Building(timeOut=10, name="B04", position=position4, consume=consume4, generate=generate4)

activate(b, b.produce())
activate(b2, b2.produce())
activate(b3, b3.produce())
activate(b4, b4.produce())


city[position] = b
city[position2] = b2
city[position3] = b3
city[position4] = b4

simulate(until=TIME)
"""